# --------------------------------------------------------------------------
# ---------------------------  Machine learning  ----------------------------
# --------------------------------------------------------------------------
#  Main of the regression model sensitivity evaluator
#  Author: J. Wrzodek
#  2020
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
from extplotlib import plot_fun_in_figure, plot_pts_in_figure
from linreglib import polynomial, least_squares, regularized_least_squares
import numpy as np
import matplotlib.pyplot as plt


def gen_def_prob(prob, fst, snd):
    """
    :param prob: problem dictionary containing:
    'x': input train
    'y': output train
    :param fst: index of first disappearing argument
    :param snd: index of second disappearing argument
    :return: defected problem dictionary
    """
    res = {
        'x': prob['x'].copy(),
        'y': prob['y'].copy()
    }
    res['x'].pop(fst)
    res['x'].pop(snd - 1)
    res['y'].pop(fst)
    res['y'].pop(snd - 1)
    return res


def gen_def_prob_lst(prob):
    """
    :param prob: problem dictionary containing:
    'x': input train
    'y': output trains
    :return: list of defected problem dictionaries
    """
    res = []
    for i in range(len(prob['x'])):
        for j in range(len(prob['x'])):
            if i != j:
                res.append(gen_def_prob(prob, i, j))
    return res


def get_main_prob_from_file(name):
    """
    :param name: name with expansion of file to load
    :return: main problem dictionary containing:
    'x': input train
    'y': output trains
    """
    with open(name) as file:
        line_lst = file.readlines()
    x_train = line_lst[0].split(',')
    x_train[-1] = x_train[-1].split('\n')[0]
    y_train = line_lst[1].split(',')
    y_train[-1] = y_train[-1].split('\n')[0]
    x_train = [float(x) for x in x_train]
    y_train = [float(y) for y in y_train]
    return {
        'x': x_train,
        'y': y_train
    }


if __name__ == "__main__":
    print(' Regression model sensitivity evaluator by J. Wrzodek \n')

    is_readed = False
    while not is_readed:
        print(' Enter name with expansion of file to load:')
        try:
            prob_name = input()
            print('\n Enter value of regulation lambda:')
            reg_lambda = float(input())
            if reg_lambda <= 0:
                raise NameError('Wrong lambda parameter')
            main_prob = get_main_prob_from_file(prob_name)
            def_prob_lst = gen_def_prob_lst(main_prob)
            is_readed = True
        except:
            print('\n ERR: Wrong input values!')
            print('   - File have to exist in main.py folder')
            print('   - File have to contains 2 lines (input train, and output train)')
            print('   - Values in trains have to be floats and be separated by comma')
            print('   - Regulation lambda have to be positive float\n')
            is_readed = False

    x_model = np.arange(min(main_prob['x']) - 1, max(main_prob['x']) + 1, 0.01)
    fig1 = plt.figure(figsize=(8, 10), num='Regression model sensitivity evaluator')
    for i in range(1, 7):
        sub = fig1.add_subplot(3, 2, i)
        for prob in def_prob_lst:
            a, err = least_squares(prob['x'], prob['y'], i)
            y_model = polynomial(x_model, a)
            plot_fun_in_figure(sub, prob['x'], prob['y'], x_model, y_model, color='orange')
        a, err = least_squares(main_prob['x'], main_prob['y'], i)
        sub.set_title(f"number of parameters: {i + 1} \n matching error: {err} \n Q(a) values: \n {a}")
        y_model = polynomial(x_model, a)
        plot_fun_in_figure(sub, main_prob['x'], main_prob['y'], x_model, y_model, color='black', width=2)
        plot_pts_in_figure(sub, main_prob['x'], main_prob['y'], color='blue')

    plt.tight_layout()
    plt.draw()
    print('\n Regression model sensitivity evaluator plotted')
    print('\n--- Enter a button to continue  ---')
    plt.waitforbuttonpress(10)

    fig2 = plt.figure(figsize=(8, 10), num='Regression model sensitivity with regularization')
    for i in range(1, 7):
        sub = fig2.add_subplot(3, 2, i)
        for prob in def_prob_lst:
            a, err = regularized_least_squares(prob['x'], prob['y'], i, reg_lambda)
            y_model = polynomial(x_model, a)
            plot_fun_in_figure(sub, prob['x'], prob['y'], x_model, y_model, color='orange')
        a, err = regularized_least_squares(main_prob['x'], main_prob['y'], i, reg_lambda)
        sub.set_title(f"number of parameters: {i + 1} \n matching error: {err} \n Q(a) values: \n {a}")
        y_model = polynomial(x_model, a)
        plot_fun_in_figure(sub, main_prob['x'], main_prob['y'], x_model, y_model, color='black', width=2)
        plot_pts_in_figure(sub, main_prob['x'], main_prob['y'], color='blue')

    plt.tight_layout()
    plt.draw()
    print('\n Regression model sensitivity evaluator plotted with regularization')
    print('\n--- Enter a button to continue  ---')
    plt.waitforbuttonpress(600)
