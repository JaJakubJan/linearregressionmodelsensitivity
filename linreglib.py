# --------------------------------------------------------------------------
# ---------------------------  Machine learning  ----------------------------
# --------------------------------------------------------------------------
#  Regression primary function module
#  Author: J. Wrzodek
#  2020
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
import numpy as np


def polynomial(x, a):
    """
    :param x: arguments vector N x 1
    :param a: arguments vector (M + 1 ) x 1
    :return: vector of  polynomial values in points x, N x 1
    """
    dm = [a[i] * x ** i for i in range(np.shape(a)[0])]
    return np.sum(dm, axis=0)


def mean_squared_error(x, y, a):
    """
    :param x: input train N x 1
    :param y: output train N x 1
    :param a: mathematical model parameters (M + 1) x 1
    :return: mean squared error between outputs y and outputs y'
    combined from polynomial a for inputs x
    """
    return (len(x)**-1 * sum(map(
        lambda xi, yi: (yi - polynomial(xi, a))**2, x, y)))[0]


def design_matrix(x_train, M):
    """
    :param x_train: training input train N x 1
    :param M: polynomial degree
    :return: design matrix N x (M + 1) for m degree polynomial
    """
    matrix = np.ones(shape=(len(x_train), M + 1))
    for i in range(len(x_train)):
        for j in range(0, M):
            matrix[i][j + 1] = x_train[i]**(j + 1)
    return matrix


def least_squares(x_train, y_train, M):
    """
    :param x_train: training inputs train N x 1
    :param y_train: training outputs train N x 1
    :param M: polynomial degree
    :return: tuple (a, err), a is the parameters of matched polynomial,
    err is the mean squared matching error
    """
    X = design_matrix(x_train, M)
    a = np.linalg.inv(X.transpose() @ X) @ \
        X.transpose() @ np.reshape(y_train, (len(y_train), 1))
    return a, mean_squared_error(x_train, y_train, a)


def regularized_least_squares(x_train, y_train, M, reg_lambda):
    """
    :param x_train: training inputs train N x 1
    :param y_train: training outputs train N x 1
    :param M: polynomial degree
    :param reg_lambda: regularization parameter
    :return: tuple (a, err), a is the parameters of matched polynomial,
    err is the mean squared matching error with the l2 regularization
    """
    X = design_matrix(x_train, M)
    I = np.identity(M + 1)
    a = np.linalg.inv(X.transpose() @ X + (I * reg_lambda)) \
        @ X.transpose() @ np.reshape(y_train, (len(y_train), 1))
    return a, mean_squared_error(x_train, y_train, a)


def model_selection(x_train, y_train, x_val, y_val, M_values):
    """
    :param x_train: training inputs train N x 1
    :param y_train: training outputs train N x 1
    :param x_val: validation inputs train N x 1
    :param y_val: validation inputs train N x 1
    :param M_values: list of polynomial degrees
    :return: tuple (a, train_err, val_err), a is the parameters of matched polynomial,
    train_err is the mean squared matching error for training trains,
    val_err is the mean squared matching error for validation trains
    """
    a_train = [least_squares(x_train, y_train, M)[0] for M in M_values]
    err_train = [mean_squared_error(x_train, y_train, a) for a in a_train]
    val_err = [mean_squared_error(x_val, y_val, a) for a in a_train]
    idx = val_err.index(min(val_err))
    return a_train[idx], err_train[idx], val_err[idx]


def regularized_model_selection(x_train, y_train, x_val, y_val, M, lambda_values):
    """
    :param x_train: training inputs train N x 1
    :param y_train: training outputs train N x 1
    :param x_val: validation inputs train N x 1
    :param y_val: validation inputs train N x 1
    :param M: polynomial degree
    :param lambda_values: list of regularization parameters
    :return: tuple (a, train_err, val_err, reg_lambda), a is the parameters of matched polynomial,
    train_err is the mean squared matching error for training trains,
    val_err is the mean squared matching error for validation trains
    reg_lambda: best regularization parameter
    """
    a_train = [regularized_least_squares(x_train, y_train, M, lam)[0] for lam in lambda_values]
    train_err = [mean_squared_error(x_train, y_train, a) for a in a_train]
    val_err = [mean_squared_error(x_val, y_val, a) for a in a_train]
    idx = val_err.index(min(val_err))
    return a_train[idx], train_err[idx], val_err[idx], lambda_values[idx]
