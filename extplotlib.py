# --------------------------------------------------------------------------
# ---------------------------  Machine learning  ----------------------------
# --------------------------------------------------------------------------
#  Extended plot function module
#  Author: J. Wrzodek
#  2020
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
import numpy as np


def prep_plot(fig, x_vls, y_vls, x_res=6, y_res=6, ndigits=2):
    """
    :param x_vls: train of x values to plot
    :param y_vls: train of y values to plot
    :param x_res: resolution of x values
    :param y_res: resolution of y values
    :param ndigits: number of digits to round
    """
    x_min = round((min(x_vls)), ndigits)
    x_max = round((max(x_vls)), ndigits)
    y_min = round((min(y_vls)), ndigits)
    y_max = round((max(y_vls)), ndigits)
    x_step = round(((x_max - x_min) / x_res), ndigits)
    y_step = round(((y_max - y_min) / y_res), ndigits)
    x_ticks = [x for x in np.arange(x_min - x_step, x_max + x_step, x_step)]
    y_ticks = [y for y in np.arange(y_min - y_step, y_max + y_step, y_step)]
    fig.set_xlim(x_min - x_step, x_max + x_step)
    fig.set_ylim(y_min - y_step, y_max + x_step)
    fig.set_xticks(x_ticks)
    fig.set_yticks(y_ticks)


def plot_fun_in_figure(fig, x_vls, y_vls, x_mod, y_mod, color='red', width=0.4, ndigits=2, x_res=6, y_res=6):
    """
    :param fig: figure to plot
    :param x_vls: train of x values to plot
    :param y_vls: train of y values to plot
    :param x_mod: list of domain values (digital)
    :param y_mod: list of function y results
    :param color: color of plotted line
    :param width: width of plotted line
    :param ndigits: number of digits to round
    :param x_res: resolution of x values
    :param y_res: resolution of y values
    """
    prep_plot(fig, x_vls, y_vls, x_res, y_res, ndigits)
    fig.plot(x_mod, y_mod, '-r', linewidth=width, color=color)


def plot_pts_in_figure(fig, x_vls, y_vls, color='red', size=4, ndigits=2, x_res=6, y_res=6):
    """
    :param fig: figure to plot
    :param x_vls: train of x values to plot
    :param y_vls: train of y values to plot
    :param color: color of plotted points
    :param size: size of plotted points
    :param ndigits: number of digits to round
    :param x_res: resolution of x values
    :param y_res: resolution of y values
    """
    prep_plot(fig, x_vls, y_vls, x_res, y_res, ndigits)
    fig.plot(x_vls, y_vls, 'o', markerfacecolor=color, markeredgecolor=color, markersize=size, markeredgewidth=size/2)